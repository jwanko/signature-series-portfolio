module.exports = {
    js: {
        files: [
            {
                expand: true,
                cwd: 'html/src/scripts/libs',
                src: ['*'],
                dest: 'html/scripts',
            }

        ]
    }
};