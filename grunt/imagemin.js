module.exports = {
    all: {
        files: [{
            expand: true,
            cwd: 'html/src/',
            src: ['images/**/*.{png,jpg,gif}'],
            dest: 'html/'
        }]
    }
};