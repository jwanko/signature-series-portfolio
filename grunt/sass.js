module.exports = {
    // Development settings
    dev: {
        options: {
            outputStyle: 'nested',
            sourceMap: false
        },
        files: [{
            expand: true,
            cwd: 'html/src/styles',
            src: ['*.scss'],
            dest: 'html/styles',
            ext: '.css'
        }]
    },
    // Production settings
    prod: {
        options: {
            outputStyle: 'compressed',
            sourceMap: false
        },
        files: [{
            expand: true,
            cwd: 'html/src/styles',
            src: ['*.scss'],
            dest: 'html/styles',
            ext: '.css'
        }]
    }
};