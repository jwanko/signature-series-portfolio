module.exports = {
    styles: {
        files: [
            'html/src/styles/**/**/**/*.scss',
        ],
        tasks: [
            'concurrent:devStylesFirst',
            'concurrent:devStylesSecond',
            ]
    },
    scripts: {
        files: [
            'html/src/scripts/**/*.js'
        ],
        tasks: [
            'concurrent:devScriptsFirst',
            'concurrent:devScriptsSecond',
            ]
    },
    img: {
        files: [
            'html/src/images/**/*'
        ],
        tasks: [
            'concurrent:imgFirst',
            'concurrent:imgSecond',
        ]
    }
};