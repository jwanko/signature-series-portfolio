module.exports = {
    options: {
        mangle: {
            except: ['$scope']
        }
    },
    prod: {
        options : {
            sourceMap : true,
            sourceMapName : 'html/scripts/sourceMap.map'
        },
        files: [{
            src : [
                'html/src/scripts/*.js',
                'html/src/scripts/global/**/*.js',
                '!html/src/scripts/libs/*.js'
            ],
            dest : 'html/scripts/main.min.js'
        }]
    },
    dev: {
        options : {
            sourceMap : false,
            sourceMapName : 'html/scripts/sourceMap.map',
            compress : false,
            beautify : false
        },
        files: [{
            src : [
                'html/src/scripts/*.js',
                'html/src/scripts/global/**/*.js',
                '!html/src/scripts/libs/*.js'
            ],
            dest : 'html/scripts/main.min.js'
        }]
    }
};