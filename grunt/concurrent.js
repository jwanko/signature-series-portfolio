module.exports = {

    // Task options
        options: {
            limit: 4,
            logConcurrentOutput: true
        },

    // Dev tasks

        // Styles
            devStylesFirst: [
                'clean:styles',
            ],
            devStylesSecond: [
                'sass:dev',
            ],
            devStylesThird: [
                'autoprefixer',
            ],

        // Scripts
            devScriptsFirst: [
                'clean:scripts',
                'jshint',
            ],
            devScriptsSecond: [
                'copy:js',
                'uglify:dev'
            ],

    // Production tasks
        prodFirst: [
            'clean:styles',
            'clean:scripts',
            'jshint'
        ],
        prodSecond: [
            'copy:js',
            'sass:prod',
            'uglify:prod'
        ],
        prodThird: [
            'autoprefixer',
            'svgstore',
        ],

    // Image tasks
        imgFirst: [
            'clean:img',
        ],
        imgSecond: [
            'imagemin',
            'svgstore'
        ],

    // Watch Task
        watch: [
            'watch:img',
            'watch:styles',
            'watch:scripts',
        ],

};